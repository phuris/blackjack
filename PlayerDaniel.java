public class PlayerDaniel extends Player{


        PlayerDaniel(){
            super();
        }

        public String playersMove(String command){
            int score = this.getHandScore();
            if(score <= 12){
                return CommandFactory.instance().parse("hit", this).execute();
            }else{
                return CommandFactory.instance().parse("stand", this).execute();
            }
        }


}
