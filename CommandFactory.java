public class CommandFactory {
    private static CommandFactory theInstance;


    public static synchronized CommandFactory instance() {
        if (theInstance == null) {
            theInstance = new CommandFactory();
        }
        return theInstance;
    }
    private CommandFactory() {
    }


    public Command parse(String command, Player player) {

        if(command.equalsIgnoreCase("hit")){
            return new HitCommand(player);
        }
        else if(command.equalsIgnoreCase("stand")){
            return new StandCommand(player);
        }
        
        //TODO tie this in if We decide not the have bet just be a prompt with a pot on the table
        else if(command.equalsIgnoreCase("bet")){
            return new BetCommand(player);
        }else{
            return new UnknownCommand(command);
        }
    }

}
