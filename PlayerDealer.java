import java.util.ArrayList;

public class PlayerDealer extends Player{



    PlayerDealer(){
        super();
    }



    //Deals two cards to each player one at a time in an order.
    @Override
    public void deal(ArrayList<Player> players){
        for(int i = 0; i<2;i++) {
            for (Player player : players) {
                player.addCard(GameState.instance().getDeck().getACard());
            }
        }
    }

    //displays the name of the dealers upcard
    public String getUpCard(){
         return super.getHand().get(0).getName();
    }


    public boolean doesPlayerHit() {

        int points = getHandScore();
        // implement your strategy for deciding whether to hit or not
        if(points>17) {
            return false;
        } else {
            return true;
        }
    }


    //dealers strategy
    public String playersMove(){
        int points = getHandScore();
        // implement your strategy for deciding whether to hit or not
        if(points<17) {
            return CommandFactory.instance().parse("hit", this).execute();
        } else {
            return CommandFactory.instance().parse("stand", this).execute();
        }

    }

    @Override
    public void placeBet(){

    }


}
