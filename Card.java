public class Card {

    private int points;
    private String name;


    Card(int points, String name){
        this.points = points;
        this.name = name;

    }

    public int getPoints() {
        return points;
    }

    public String getName() {
        return name;
    }
}
