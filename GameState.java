import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class GameState {

    private static GameState theInstance;

    private Deck deck;
    private ArrayList<Player> players;
    private Player dealer;

    private GameState() {
        this.players = new ArrayList<>();
        int numberOfDecks = 8;
        this.deck = new Deck(numberOfDecks);
        this.dealer = new PlayerDealer();


    }

    //SINGLETON to represent the table (GameState)
    static synchronized GameState instance() {
        if (theInstance == null) {
            theInstance = new GameState();
        }
        return theInstance;
    }

    void restore(String filename) throws FileNotFoundException {
        Scanner scnr = new Scanner(new FileReader(filename));


    }

    public Deck getDeck(){
        return deck;
    }

    public ArrayList<Player> getPlayers(){
        return this.players;
    }


    public void addPlayer(Player player){
        players.add(player);
    }

    public void removePlayer(Player player){ players.remove(player);
    }

    //resets the dealer to default state
    private void resetDealer(){
        removePlayer(dealer);
        this.dealer = new PlayerDealer();
        addPlayer(dealer);
    }


    public Player getDealer(){
        return dealer;
    }

    //Starts a new round
    //Empties hands and makes a new dealer
    public void newDeal() {
        resetDealer();
        for(Player player: this.players){
            player.emptyHand();
        }
        //Gives everyone in game two cards
        dealer.deal(players);
    }

    //Called at the end of a round of play to perform all actions associated with ending a round.
    //Gathers scores and compares them to dealer
    //prints results
    public void finishRound() {
        int i = 1;
        for(Player player: players){
            int score = player.getHandScore();
            if(player != dealer) {
                System.out.print("Player " + i + ": ");
                i++;
            }else{
                System.out.print("Dealer: ");
            }
            System.out.println("\n" + player.displayHand());
            System.out.println("Score: " + score);
            System.out.println("**************************");
            if(score > dealer.getHandScore() && player != dealer && score < 22){
                System.out.println("You Won");
                //TODO write a method that delivers winnings to players who beat dealer.
                player.youWon();
            }
        }


    }

    public void dealersTurn() {
        dealer.playersMove("filler");
    }
}
