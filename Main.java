import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String command;
        Scanner commandLine = new Scanner(System.in);
        Player member;
        System.out.println("Welcome to BlackJack!");

        try{
            if(args.length > 0) {
                if (args[0].endsWith(".sav")) {
                    //load old game data
                    GameState.instance().restore(args[0]);
                    member = new PlayerManual();//TODO chage this from test to actual reading of momento
                }else {
                    for(String arg: args) {
                        member = (Player) Class.forName(arg).newInstance();
                        GameState.instance().addPlayer(member);

                    }
                }
            }else{
                member = new PlayerManual();
                GameState.instance().addPlayer(member);

            }


            //load hand
            System.out.println("type:\n'q' to quit\n'hit' to hit\n'stand' to stand");
            System.out.println("****************************");

            GameState.instance().newDeal();
            System.out.println("ready(y/q)?");
            command = promptUser(commandLine);
            boolean bustOrStand;

            while(!command.equals("q")){
                for(Player player: GameState.instance().getPlayers()){
                bustOrStand = false;
                    System.out.println(player + " turn!");
                    System.out.println("Your chip total is $" + player.getBank() );
                    System.out.println("******************************");



                    player.placeBet();
                    System.out.println("Your chip total is $" + player.getBank() );
                    System.out.println("******************************");
                while(!bustOrStand) {

                    //Display current round info
                    System.out.println("Your hand:");
                    System.out.println(player.displayHand());
                    System.out.println("Dealers up card : " + GameState.instance().getDealer().getUpCard());
                    //Comment out the line below if you want it to auto play
                    command = promptUser(commandLine);

                    //hacky way of seeing if a player stood should probably do better 
                    int score = player.getHandScore();
                    System.out.println(player.playersMove(command));
                    if ((player.getHandScore() > 21)) {
                        bustOrStand = true;
                        System.out.println("You bust with " + player.getHandScore());
                    } else if (score == player.getHandScore()) {
                        bustOrStand = true;
                    }
                }
                }
                //Run dealers turn
                GameState.instance().dealersTurn();
                //finish up round
                GameState.instance().finishRound();
                //deal next hand
                GameState.instance().newDeal();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private static String promptUser(Scanner commandLine) {
        System.out.print("> ");
        return commandLine.nextLine();
    }
}
