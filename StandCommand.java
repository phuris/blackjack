public class StandCommand extends Command {
    private Player player;

    StandCommand(Player player){
        this.player = player;

    }
    StandCommand(){

    }


    String execute(){
        return "Player Stands at " + player.getHandScore();
    }


}