public class HitCommand extends Command {
    private Player player;

    HitCommand(Player player){
        this.player = player;
    }

    String execute(){
        player.addCard(GameState.instance().getDeck().getACard());
        return "you hit!";
    }


}
