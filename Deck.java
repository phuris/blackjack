import java.util.Random;

public class Deck {

    private int totalNumberOfCards;

    private int numAce;
    private int numKing;
    private int numQueen;
    private int numJack;
    private int numTen;
    private int numNine;
    private int numEight;
    private int numSeven;
    private int numSix;
    private int numFive;
    private int numFour;
    private int numThree;
    private int numTwo;



    Deck(int decks) {
        this.totalNumberOfCards = decks * 52;
        this.numAce = 4 * decks;
        this.numKing = 4 * decks;
        this.numQueen = 4 * decks;
        this.numJack = 4 * decks;
        this.numTen = 4 * decks;
        this.numNine = 4 * decks;
        this.numEight = 4 * decks;
        this.numSeven = 4 * decks;
        this.numSix = 4 * decks;
        this.numFive = 4 * decks;
        this.numFour = 4 * decks;
        this.numThree = 4 * decks;
        this.numTwo = 4 * decks;
    }

    Card getACard(){
        Random rand = new Random();
        int cardNum = rand.nextInt(totalNumberOfCards)+1;
        this.totalNumberOfCards--;

        if(totalNumberOfCards == 0){
            System.out.println("four new decks coming in!");
            this.totalNumberOfCards = 208;
        }

        int range = 0;

        if(cardNum < range+numTwo ){
            numTwo--;
            return new Card(2, "Two");
        }else{
            range +=numTwo;
        }

        if(cardNum < range+numThree ){
            numThree--;
            return new Card(3, "Three");
        }else{
            range +=numThree;
        }

        if(cardNum < range+numFour ){
            numFour--;
            return new Card(4, "Four");
        }else{
            range +=numFour;
        }
        if(cardNum < range+numFive ){
            numFive--;
            return new Card(5, "Five");
        }else{
            range += numFive;
        }
        if(cardNum < range+numSix ){
            numSix--;
            return new Card(6, "Six");
        }else{
            range +=numSix;
        }
        if(cardNum < range+numSeven ){
            numSeven--;
            return new Card(7, "Seven");
        }else{
            range +=numSeven;
        }
        if(cardNum < range+numEight ){
            numEight--;
            return new Card(8, "Eight");
        }else{
            range +=numEight;
        }
        if(cardNum < range+numNine ){
            numNine--;
            return new Card(9, "Nine");
        }else{
            range +=numNine;
        }
        if(cardNum < range+numTen ){
            numTen--;
            return new Card(10, "Ten");
        }else{
            range +=numTen;
        }
        if(cardNum < range+numJack ){
            numJack--;
            return new Card(10, "Jack");
        }else{
            range +=numJack;
        }
        if(cardNum < range+numQueen ){
            numQueen--;
            return new Card(10, "Queen");
        }else{
            range +=numQueen;
        }
        if(cardNum < range+numKing ){
            numKing--;
            return new Card(10, "King");
        }else{
            range +=numKing;
        }
        if(cardNum < range+numAce ){
            numAce--;
            return new Card(11, "Ace");
        }else{
            range +=numAce;
        }
        return new Card(99, "Card failed");
    }



}
