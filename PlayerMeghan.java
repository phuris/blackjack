
public class PlayerMeghan extends Player {
	
	PlayerMeghan(){
        super();
    }

    public String playersMove(String command){
        int score = this.getHandScore();
        if(score <15){
            return CommandFactory.instance().parse("hit", this).execute();
        }else{
            return CommandFactory.instance().parse("stand", this).execute();
        }
    }



}
