class UnknownCommand extends Command {

    private String command;

    UnknownCommand(String command) {
        this.command = command;
    }

    String execute() {
        return "\"" +  command + "\" is not a valid command";
    }
}

